import {
    ADD_DISHES_CART,
    ADD_DISHES_FAILURE,
    ADD_DISHES_REQUEST,
    ADD_DISHES_SUCCESS,
    DELETE_DISHES_FAILURE,
    DELETE_DISHES_REQUEST,
    DELETE_DISHES_SUCCESS,
    EDIT_DISH_FAILURE,
    EDIT_DISH_REQUEST,
    EDIT_DISH_SUCCESS,
    FETCH_DISH_FAILURE,
    FETCH_DISH_REQUEST,
    FETCH_DISH_SUCCESS,
    FETCH_DISHES_FAILURE,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
    SET_ID,
    SET_MODAL_OPEN
} from "../actions/dishesActions";

const initialState = {
    dish: {},
    dishes: {},
    loading: false,
    error: null,
    showPurchaseModal: false,
    id: '',
    totalPrice: 0,
    orderDetail: {},
}
const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISHES_REQUEST:
            return {...state, loading: true};
        case ADD_DISHES_SUCCESS:
            return {
                ...state, loading: false,
                dishes: {
                    ...state.dishes,
                    ...action.payload
                }
            }
        case ADD_DISHES_FAILURE:
            return {...state, loading: false, error: action.payload};

        case EDIT_DISH_REQUEST:
            return {...state, loading: true};
        case EDIT_DISH_SUCCESS:
            return {
                ...state, loading: false,
                dishes: {
                    ...state.dishes[action.id],
                    ...action.payload
                }
            }
        case EDIT_DISH_FAILURE:
            return {...state, loading: false, error: action.payload};

        case FETCH_DISHES_REQUEST:
            return {...state, loading: true};
        case FETCH_DISHES_SUCCESS:
            return {...state, loading: false, dishes: action.payload};
        case FETCH_DISHES_FAILURE:
            return {...state, loading: false, error: action.payload};
        case DELETE_DISHES_REQUEST:
            return {...state, loading: true};

        case DELETE_DISHES_SUCCESS:
            const dishes = {...state.dishes};
            delete dishes[action.payload];
            return {...state, loading: false, dishes: dishes};

        case DELETE_DISHES_FAILURE:
            return {...state, loading: false, error: action.payload};
        case SET_MODAL_OPEN:
            return {
                ...state, showPurchaseModal: action.payload
            };

        case FETCH_DISH_REQUEST:
            return {...state, loading: true};
        case FETCH_DISH_SUCCESS:
            return {...state, loading: false, dish: action.payload};
        case FETCH_DISH_FAILURE:
            return {...state, loading: false, error: action.payload};
        case SET_ID:
            return {...state, id: action.payload};
        case ADD_DISHES_CART:
            return {
                ...state, totalPrice: state.totalPrice + parseInt(state.dishes[action.payload].price), orderDetail: {
                    id: action.payload,
                    title: action.title,
                    price: action.price,
                }
            };
        default:
            return state;
    }
};

export default dishesReducer;