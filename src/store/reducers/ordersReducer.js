import {SEND_ORDER_FAILURE, SEND_ORDER_REQUEST, SEND_ORDER_SUCCESS} from "../actions/ordersAction";

const initialState = {
    loading: false,
    fetchLoading: false,
    error: null,
    fetchError: null,
    orders: null,
};

const ordersReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_ORDER_REQUEST:
            return {...state, loading: true};
        case SEND_ORDER_SUCCESS:
            return {...state, loading: false, orders: action.payload};
        case SEND_ORDER_FAILURE:
            return {...state, loading: false, error: action.payload};
        // case FETCH_ORDERS_REQUEST:
        //     return {...state, fetchLoading: true, fetchError: null};
        // case FETCH_ORDERS_SUCCESS:
        //     return {...state, orders: action.payload, fetchLoading: false};
        // case FETCH_ORDERS_FAILURE:
        //     return {...state, fetchError: action.payload, fetchLoading: false}
        default:
            return state;
    }
};

export default ordersReducer;