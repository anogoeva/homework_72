import axiosApi from "../../axiosApi";

export const ADD_DISHES_REQUEST = 'ADD_DISHES_REQUEST';
export const ADD_DISHES_SUCCESS = 'ADD_DISHES_SUCCESS';
export const ADD_DISHES_FAILURE = 'ADD_DISHES_FAILURE';

export const EDIT_DISH_REQUEST = 'EDIT_DISH_REQUEST';
export const EDIT_DISH_SUCCESS = 'EDIT_DISH_SUCCESS';
export const EDIT_DISH_FAILURE = 'EDIT_DISH_FAILURE';

export const FETCH_DISHES_REQUEST = 'FETCH_DISHES_REQUEST';
export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISHES_FAILURE = 'FETCH_DISHES_FAILURE';


export const DELETE_DISHES_REQUEST = 'DELETE_DISHES_REQUEST';
export const DELETE_DISHES_SUCCESS = 'DELETE_DISHES_SUCCESS';
export const DELETE_DISHES_FAILURE = 'DELETE_DISHES_FAILURE';

export const FETCH_DISH_REQUEST = 'FETCH_DISH_REQUEST';
export const FETCH_DISH_SUCCESS = 'FETCH_DISH_SUCCESS';
export const FETCH_DISH_FAILURE = 'FETCH_DISH_FAILURE';

export const ADD_DISHES_CART = 'ADD_DISHES_CART';
export const SET_MODAL_OPEN = 'SET_MODAL_OPEN';
export const SET_ID = 'SET_ID';

export const addDishesRequest = () => ({type: ADD_DISHES_REQUEST});
export const addDishesSuccess = dishesData => ({type: ADD_DISHES_SUCCESS, payload: dishesData});
export const addDishesFailure = error => ({type: ADD_DISHES_FAILURE, payload: error});

export const editDishRequest = () => ({type: EDIT_DISH_REQUEST});
export const editDishSuccess = (dishesData, id) => ({type: EDIT_DISH_SUCCESS, payload: dishesData, id: id});
export const editDishFailure = error => ({type: EDIT_DISH_FAILURE, payload: error});

export const fetchDishesRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishesSuccess = dishesData => ({type: FETCH_DISHES_SUCCESS, payload: dishesData});
export const fetchDishesFailure = error => ({type: FETCH_DISHES_FAILURE, payload: error});

export const deleteDishesRequest = () => ({type: DELETE_DISHES_REQUEST});
export const deleteDishesSuccess = id => ({type: DELETE_DISHES_SUCCESS, payload: id});
export const deleteDishesFailure = error => ({type: DELETE_DISHES_FAILURE, payload: error});

export const fetchDishRequest = () => ({type: FETCH_DISH_REQUEST});
export const fetchDishSuccess = dish => ({type: FETCH_DISH_SUCCESS, payload: dish});
export const fetchDishFailure = error => ({type: FETCH_DISH_FAILURE, payload: error});

export const setId = (id) => ({type: SET_ID, payload: id});
export const addDishesCart = (id, price, title) => ({type: ADD_DISHES_CART, payload: id, price: price, title: title});
export const setModalOpen = isOpen => ({type: SET_MODAL_OPEN, payload: isOpen});

export const createDishes = dishesData => {
    return async dispatch => {
        try {
            dispatch(addDishesRequest());
            const response = await axiosApi.post('/dishes.json', dishesData);
            if (response.data) {
                const obj = {};
                obj[response.data.name] = dishesData;
                dispatch(addDishesSuccess(obj));
            }
        } catch (e) {
            dispatch(addDishesFailure())
        }
    }
};

export const fetchDishes = () => {
    return async dispatch => {
        try {
            dispatch(fetchDishesRequest());
            const response = await axiosApi.get('/dishes.json');
            dispatch(fetchDishesSuccess(response.data));
        } catch (e) {
            dispatch(fetchDishesFailure())
        }
    }
};

export const deleteDish = (id) => {
    return async dispatch => {
        try {
            dispatch(deleteDishesRequest());
            const response = await axiosApi.delete('/dishes/' + id + '.json');
            if (response.data === null) {
                dispatch(deleteDishesSuccess(id));
            }

        } catch (e) {
            dispatch(deleteDishesFailure())
        }
    }
};

export const editDish = (dishesData, id) => {
    return async dispatch => {
        try {
            dispatch(editDishRequest());
            const response = await axiosApi.put('/dishes/' + id + '.json', dishesData);
            dispatch(editDishSuccess(response.data, id))
        } catch (e) {
            dispatch(editDishFailure())
        }
    }
};

export const fetchDishFromFirebase = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchDishRequest());
            const response = await axiosApi.get('/dishes/' + id + '.json');
            dispatch(fetchDishSuccess(response.data));
        } catch (e) {
            dispatch(fetchDishFailure());
        }
    }
};