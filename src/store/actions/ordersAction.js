// import axiosApi from "../../axiosApi";
// import Dish from "../../Components/Dish/Dish";
//
// export const FETCH_ORDERS_REQUEST = 'FETCH_ORDERS_REQUEST';
// export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
// export const FETCH_ORDERS_FAILURE = 'FETCH_ORDERS_FAILURE';
//
// export const fetchOrdersRequest = () => ({type: FETCH_ORDERS_REQUEST});
// export const fetchOrdersSuccess = orders => ({type: FETCH_ORDERS_SUCCESS, payload: orders});
// export const fetchOrdersFailure = error => ({type: FETCH_ORDERS_FAILURE, payload: error});
//
//
// export const fetchOrders = () => {
//     return async dispatch => {
//         try {
//             dispatch(fetchOrdersRequest());
//             const response = await axiosApi.get('/orders.json');
//
//             const fetchOrders = Object.keys(response.data).map(id => {
//                 const order = response.data[id];
//                 const price = Object.keys(order.dishes).reduce(acc, type) => {
//                     return acc + Dish[type].price * order.dishes[type];
//                 }, BASE_PRICE);
//
//             return {...order, id, price};
//             });
//             dispatch(fetchOrdersSuccess(fetchOrders));
//         } catch (e) {
//             dispatch(fetchOrdersFailure(e));
//         }
//     };
// }

import axiosApi from "../../axiosApi";

export const SEND_ORDER_REQUEST = 'SEND_ORDER_REQUEST';
export const SEND_ORDER_SUCCESS = 'SEND_ORDER_SUCCESS';
export const SEND_ORDER_FAILURE = 'SEND_ORDER_FAILURE';

export const sendOrderRequest = () => ({type: SEND_ORDER_REQUEST});
export const sendOrderSuccess = order => ({type: SEND_ORDER_SUCCESS, payload: order});
export const sendOrderFailure = error => ({type: SEND_ORDER_FAILURE, payload: error});

export const sendOrder = orderData => {
    return async dispatch => {
        try {
            dispatch(sendOrderRequest());
            const response = await axiosApi.post('/ordersData.json', orderData);
            if (response.data) {
                const obj = {};
                obj[response.data] = orderData;
                dispatch(sendOrderSuccess(obj));
            }
        } catch (e) {
            dispatch(sendOrderFailure())
        }
    }
};