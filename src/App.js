import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./Components/UI/Layout/Layout";
import DishesPage from "./container/DishesPage/DishesPage";
import CartPage from "./container/CartPage/CartPage";
import AddComponent from "./Components/AddComponent/AddComponent";
import EditComponent from "./Components/EditComponent/EditComponent";

const App = () => (
    <BrowserRouter>
        <Layout>
            <Switch>
                <Route path="/" exact component={DishesPage}/>
                {/*<Route path="/orders" component={Orders}/>*/}
                <Route path="/EditAdd" component={AddComponent}/>
                <Route path="/dishes/:id/edit" component={EditComponent}/>
                <Route path="/cart" component={CartPage}/>
            </Switch>
        </Layout>
    </BrowserRouter>
);

export default App;
