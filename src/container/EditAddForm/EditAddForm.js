import React, {useState} from 'react';
import './EditAddForm.css';
import {createDishes, editDish} from "../../store/actions/dishesActions";
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";

const EditAddForm = (props) => {
    const histories = useHistory();
    const dispatch = useDispatch();
    const addDishes = (e) => {
        if (histories.location.pathname.includes('/edit')) {
            e.preventDefault();
            dispatch(editDish(inputBody, histories.location.pathname.slice(8, -5)));
        } else {
            histories.push('/');
            e.preventDefault();
            dispatch(createDishes(inputBody));
        }
    };
    const [inputBody, setInputBody] = useState({
        image: '',
        price: '',
        title: '',
    });
    const onInputChange = e => {
        const {name, value} = e.target;
        setInputBody(prev => ({
            ...prev,
            [name]: value
        }));
    };
    return (
        <form onSubmit={addDishes}>
            <div className="EditAddForm">
                <h4>Title</h4>
                <input className="form"
                       name="title"
                       type="text"
                       defaultValue={props.dish ? props.dish.title : inputBody.title}
                       onChange={onInputChange}/>
                <p>Price:</p>
                <input className="form"
                       name="price"
                       type="text"
                       defaultValue={props.dish ? props.dish.price : inputBody.price}
                       onChange={onInputChange}/>
                <p>Image:</p>
                <input className="form"
                       name="image"
                       type="text"
                       defaultValue={props.dish ? props.dish.image : inputBody.img}
                       onChange={onInputChange}/>
            </div>
            <button type="submit" className="btn">Send</button>
        </form>
    );
};

export default EditAddForm;