import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addDishesCart, setModalOpen} from "../../store/actions/dishesActions";
import Modal from "../../Components/UI/Modal/Modal";
import OrderSummary from "../../Components/OrderSummary/OrderSummary";

const CartPage = () => {
    const purchaseHandler = () => {
        dispatch(setModalOpen(true));
    };
    const dispatch = useDispatch();
    const showPurchaseModal = useSelector(state => state.dishesReducer.showPurchaseModal);

    const dishes = useSelector(state => state.dishesReducer.dishes);
    const totalPrice = useSelector(state => state.dishesReducer.totalPrice);

    const countTotalPrice = (key) => {
        dispatch(addDishesCart(key));
    };

    const purchaseCancelHandler = () => {
        dispatch(setModalOpen(false));
    };

    return (
        <div>
            <Modal
                show={showPurchaseModal}
                close={purchaseCancelHandler}
            >
                <OrderSummary
                    dishes={dishes}
                    totalPrice={totalPrice}
                    onCancel={purchaseCancelHandler}
                />
            </Modal>
            {Object.entries(dishes).map(([key, value], i) => {
                return (
                    <div key={key} className="single_dish">
                        <img src={value.image} alt="" width="100" height="80" onClick={() => countTotalPrice(key)}/>
                        <h4>{value.title}</h4>
                        <p>{value.price}</p>
                    </div>
                )
            })}
            <hr/>

            <p>Total price: {totalPrice}</p>
            <button className="btn" onClick={purchaseHandler}>Checkout</button>
        </div>
    );
};

export default CartPage;