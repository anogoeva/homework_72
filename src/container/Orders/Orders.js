// import React, {useEffect} from 'react';
// import {shallowEqual, useDispatch, useSelector} from "react-redux";
// import Spinner from "../../Components/UI/Spinner/Spinner";
//
// const Orders = () => {
//     const dispatch = useDispatch();
//     const {fetchLoading, fetchError, orders} = useSelector(state => ({
//         fetchLoading: state.orders.fetchLoading,
//         fetchError: state.orders.fetchError,
//         orders: state.orders.orders,
//     }), shallowEqual);
//
//     useEffect(() => {
//         dispatch(fetchOrders());
//     }, [dispatch]);
//
//     let ordersComponents = orders.map(order => (
//         <ErrorBoundary key={order.id}>
//             <OrderItem
//                 ingredients={order.ingredients}
//                 customer={order.customer}
//                 price={order.price}
//             />
//         </ErrorBoundary>
//     ));
//     if (fetchLoading) {
//         ordersComponents = <Spinner/>;
//     } else if (fetchError) {
//         ordersComponents = <div>Sorry, could not fetch orders</div>
//     }
//
//     return ordersComponents
//
// };
//
// export default Orders;