import React, {useEffect} from 'react';
import './DishesPage.css'
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes} from "../../store/actions/dishesActions";
import Dish from "../../Components/Dish/Dish";

const DishesPage = () => {
    const dishes = useSelector(state => state.dishesReducer.dishes);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch]);

    const history = useHistory();
    const goEditAddPage = async e => {
        history.push('/EditAdd')
    };
    return (
        <div className="DishesPage">
            <div className="TopDishesLeft">
                <div className="TopDishesRight">
                    <button type="button" onClick={goEditAddPage}>Add new dish</button>
                </div>
            </div>
            <hr/>
            <div>
                <h5>Dishes</h5>
            </div>
            {dishes && Object.entries(dishes).map(([key, value], i) => {
                return (
                    <Dish
                        key={key}
                        id={key}
                        title={value.title}
                        price={value.price}
                        image={value.image}
                    />
                );
            })}
        </div>
    );
};

export default DishesPage;