import React from 'react';
import EditAddForm from "../../container/EditAddForm/EditAddForm";

const AddComponent = () => {
    return (
        <div>
            <EditAddForm/>
        </div>
    );
};

export default AddComponent;