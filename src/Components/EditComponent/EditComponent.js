import React, {useEffect} from 'react';
import EditAddForm from "../../container/EditAddForm/EditAddForm";
import {fetchDishFromFirebase} from "../../store/actions/dishesActions";
import {useDispatch, useSelector} from "react-redux";

const EditComponent = () => {
    const id = useSelector(state => state.dishesReducer.id);
    const dish = useSelector(state => state.dishesReducer.dish);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchDishFromFirebase(id));
    }, [dispatch, id]);

    return (
        <div>
            <EditAddForm dish={dish}/>
        </div>
    );
};

export default EditComponent;