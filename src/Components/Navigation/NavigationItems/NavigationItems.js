import React from 'react';
import './NavigationItems.css'
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <ul className="NavigationItem">
            <NavigationItem to="/" exact>Dishes</NavigationItem>
            <NavigationItem to="/orders">Orders</NavigationItem>
            <NavigationItem to="/cart">Cart</NavigationItem>
        </ul>
    );
};

export default NavigationItems;