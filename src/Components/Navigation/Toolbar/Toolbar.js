import React from 'react';
import './Toolbar.css';
import NavigationItems from "../NavigationItems/NavigationItems";

const Toolbar = () => {
    return (
        <header className="Toolbar">
            <div className="Title">
                <p>Turtle Pizza Admin</p>
            </div>
            <nav>
                <NavigationItems/>
            </nav>
        </header>
    );
};

export default Toolbar;