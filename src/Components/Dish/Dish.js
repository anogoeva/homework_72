import React from 'react';
import {deleteDish, fetchDishFromFirebase, setId} from "../../store/actions/dishesActions";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";
import './Dish.css';

const Dish = (props) => {

    const histories = useHistory();
    const dispatch = useDispatch();
    const deleteDishId = (e) => {
        e.preventDefault();
        dispatch(deleteDish(props.id));
    };

    const editDishID = () => {
        histories.push('/dishes/'+ props.id +'/edit');
        dispatch(fetchDishFromFirebase(props.id));
        dispatch(setId(props.id));
    };

    return (
        <div className="single_dish">
            <img className="item" src={props.image} alt="dish" width="100" height="80"/>
            <h3  className="item">{props.title}</h3>
            <p  className="item">{props.price}</p>
            <button className="itemBtn" onClick={editDishID}>&#9998;</button>
            <button className="itemBtn" onClick={deleteDishId}>&times;</button>
        </div>
    );
};

export default Dish;