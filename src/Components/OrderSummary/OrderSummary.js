import React from 'react';
import {sendOrder} from "../../store/actions/ordersAction";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";

const OrderSummary = props => {

    const histories = useHistory();
    const dispatch = useDispatch();

    const purchaseOrderHandler = (order) => {
        dispatch(sendOrder(order));
        histories.push('/');
    };

  return (
    <>
        <h4>Your order: </h4>
          <div>
            <span style={{textTransform: 'capitalize'}}>
              <p>{props.orderDetail} x 1 <button>DELETE</button></p>
            </span>
          </div>

        <hr/>
        <p>Delivery: <b>150 KGS</b></p>
        <p>Total: <b>{props.totalPrice + 150} KGS</b></p>
        <button onClick={props.onCancel}>Cancel</button>
        <button onClick={() => purchaseOrderHandler(props.totalPrice)}>Order</button>
    </>
  );
};

export default OrderSummary;