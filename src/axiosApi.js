import axios from 'axios';

const axiosApi = axios.create({
  baseURL: 'https://general-bf34d-default-rtdb.firebaseio.com'
});

export default axiosApi;